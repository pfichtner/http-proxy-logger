package com.github.pfichtner.httplogger;

import static com.google.common.base.Preconditions.checkState;
import static com.google.common.base.Strings.isNullOrEmpty;
import static com.google.common.io.FileWriteMode.APPEND;
import static com.google.common.io.Files.asByteSink;
import io.netty.buffer.ByteBuf;
import io.netty.handler.codec.http.HttpContent;
import io.netty.handler.codec.http.HttpObject;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponse;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.littleshoot.proxy.HttpFiltersAdapter;

import com.google.common.base.Throwables;

public class HttpFiltersRequestDumper extends HttpFiltersAdapter {

	private final File directory;

	public HttpFiltersRequestDumper(File directory, HttpRequest originalRequest) {
		super(originalRequest);
		this.directory = directory;
	}

	@Override
	public HttpResponse requestPost(HttpObject httpObject) {
		File target = getTarget(this.originalRequest.getUri());
		checkState(!target.exists() || target.delete(), "Could not delete %s",
				target);
		return super.requestPost(httpObject);
	}

	@Override
	public HttpObject responsePost(HttpObject httpObject) {
		if (httpObject instanceof HttpContent) {
			HttpContent httpContent = (HttpContent) httpObject;
			String uri = this.originalRequest.getUri();
			try {
				asByteSink(ensureDirectoryExists(getTarget(uri)), APPEND)
						.write(readFully(httpContent));
			} catch (IOException e) {
				throw new IllegalStateException("Could not write " + uri, e);
			}
		}
		return super.responsePost(httpObject);
	}

	private File ensureDirectoryExists(File target) {
		File dir = target.getParentFile();
		checkState(dir.exists() || dir.mkdirs(), "Could not create %s", dir);
		return target;
	}

	private File getTarget(String urlpath) {
		try {
			URI uri = new URI(urlpath);
			return subdir(this.directory, uri.getScheme(), uri.getHost(),
					getPath(uri));
		} catch (URISyntaxException e) {
			throw Throwables.propagate(e);
		}
	}

	private static String getPath(URI uri) {
		return isNullOrEmpty(uri.getQuery()) ? uri.getPath() : uri.getPath()
				+ "?" + uri.getQuery();
	}

	private static File subdir(File base, String... subdirs) {
		File result = base;
		for (String subdir : subdirs) {
			result = new File(result, subdir);
		}
		return result;
	}

	private static byte[] readFully(HttpContent httpContent) {
		return readFully(httpContent.content());
	}

	private static byte[] readFully(ByteBuf byteBuf) {
		byte[] bytes = new byte[byteBuf.readableBytes()];
		byteBuf.readBytes(bytes);
		return bytes;
	}

}
