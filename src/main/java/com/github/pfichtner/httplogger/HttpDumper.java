package com.github.pfichtner.httplogger;

import io.netty.handler.codec.http.HttpRequest;

import java.io.File;
import java.util.Queue;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.OptionHandlerFilter;
import org.littleshoot.proxy.ChainedProxy;
import org.littleshoot.proxy.ChainedProxyManager;
import org.littleshoot.proxy.HttpProxyServerBootstrap;
import org.littleshoot.proxy.extras.SelfSignedMitmManager;
import org.littleshoot.proxy.impl.DefaultHttpProxyServer;

public class HttpDumper {

	@Option(name = "-p", usage = "port to listen", required = false)
	private int port = 8080;

	@Option(name = "-d", usage = "directory to write files to", required = true)
	private File directory;

	@Option(name = "-t", usage = "switch for transparent proxing", required = false)
	private boolean transparent;

	@Option(name = "-m", usage = "man in the middle (mitm) support", required = false)
	private boolean mitm;

	public static void main(String[] args) {
		new HttpDumper().doMain(args);
	}

	private void doMain(String[] args) {
		CmdLineParser cmdLineParser = new CmdLineParser(this);

		try {
			cmdLineParser.parseArgument(args);
		} catch (CmdLineException e) {
			System.err.println(e.getMessage());
			cmdLineParser.printUsage(System.err);
			cmdLineParser.printExample(OptionHandlerFilter.ALL);
			return;
		}

		HttpProxyServerBootstrap bootstrap = DefaultHttpProxyServer.bootstrap()
				.withPort(this.port).withTransparent(this.transparent);
		if (this.mitm) {
			bootstrap = bootstrap.withChainProxyManager(emptyChain())
					.withManInTheMiddle(new SelfSignedMitmManager());
		}
		bootstrap.withFiltersSource(
				new HttpFiltersSourceRequestDumper(this.directory)).start();
	}

	private static ChainedProxyManager emptyChain() {
		return new ChainedProxyManager() {
			public void lookupChainedProxies(HttpRequest request,
					Queue<ChainedProxy> queue) {
			}
		};
	}

}
