package com.github.pfichtner.httplogger;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.HttpRequest;

import java.io.File;

import org.littleshoot.proxy.HttpFilters;
import org.littleshoot.proxy.HttpFiltersSourceAdapter;

public class HttpFiltersSourceRequestDumper extends HttpFiltersSourceAdapter {

	private final File directory;

	public HttpFiltersSourceRequestDumper(File directory) {
		this.directory = directory;
	}

	public HttpFilters filterRequest(HttpRequest request,
			ChannelHandlerContext ctx) {
		return new HttpFiltersRequestDumper(this.directory, request);
	}

}
